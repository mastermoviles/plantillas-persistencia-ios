//
//  SQLiteManager.m
//  PruebaSQLite
//
//  Created by Otto Colomina Pardo on 31/12/14.
//  Copyright (c) 2014 Universidad de Alicante. All rights reserved.
//

#import <sqlite3.h>
#import "DBManager.h"



@implementation DBManager

- (instancetype)initWithDB:(NSString *)nombre reload:(BOOL)reload {
    self = [super init];
    if (self) {
        NSURL *urlDB = [self copiarDB:nombre forzarCopia:reload];
        if (sqlite3_open([[urlDB path] UTF8String], &db) == SQLITE_OK) {
            NSLog(@"BD abierta"); }
        else {
            NSLog(@"Error al abrir BD: '%s'.", sqlite3_errmsg(db));
        }
    }
    return self;
}



- (NSURL *) copiarDB:(NSString *)nombre  forzarCopia:(BOOL)machaca {
    BOOL existe, ok;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //Obtenemos la URL del directorio 'Documents'
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory
                                inDomains:NSUserDomainMask];
    //La copia de la BD estará en este directorio, con el nombre especificado
    //y extensión '.db'
    NSString *nombreFichDB = [NSString stringWithFormat:@"%@.db", nombre];
    NSURL *copiaURL = [urls[0] URLByAppendingPathComponent:nombreFichDB];
    //¿Existe ya esta copia?
    existe = [fileManager fileExistsAtPath:[copiaURL path]];
    //Si existe y no hay que "machacarla" no hacer nada, devolver la URL
    if (existe && !machaca) {
        NSLog(@"Usando la BD que ya está en Documents");
        return copiaURL;
    }
    else { //copiar BD de bundle -> documents
        //URL de la BD en el bundle
        NSURL *dbURL = [[NSBundle mainBundle]
                          URLForResource: nombre withExtension:@"db"];
        //Copiar el archivo de la BD
        NSError *error;
        ok = [fileManager copyItemAtURL:dbURL toURL:copiaURL error:&error];
        if (!ok) {
            NSLog(@"No se ha podido crear una copia de la BD:'%@'.", [error localizedDescription]);
            return nil;
        }
        else
            return copiaURL;
    }
}

- (void) dealloc {
    sqlite3_close(db);
}

@end
